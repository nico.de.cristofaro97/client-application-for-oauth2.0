import React, { Component } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
//import { GetExtraInfo } from "../server/resourceServer";
//import { GetSecretAgent } from "../server/resourceServer";
import { GetNews } from "../server/resourceServer";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../customCSS/welcome.css";

class WelcomePage extends Component {
  constructor(props) {
    super(props);
    this.callbackInfoGot = this.callbackInfoGot.bind(this);
    this.callbackServerError = this.callbackServerError.bind(this);
    this.callbackUnauthorizedError = this.callbackUnauthorizedError.bind(this);

    this.state = {
      serverError: null
    };
  }

  callbackInfoGot(info) {
    //console.log(info);
    this.props.onGetInfo(info);
  }

  callbackServerError(data) {
    //console.log(Object.keys(data));
    this.setState({
      serverError: {
        error: data.error,
        message: data.error_description
      }
    });
  }

  callbackUnauthorizedError(data) {
    //error 401 Unauthorized
    this.props.onUnauthorizedError(data);
  }

  doGet(props) {
    //this was the general request to understand the flow
    //this request get from the access token the extraInfo->siteId
    /*GetExtraInfo(
      this.props.accessToken,
      this.callbackInfoGot,
      this.callbackServerError,
      this.callbackUnauthorizedError
    );*/
    //this new request to the resource server get the data of a secret agent
    /*GetSecretAgent(
      "007", // agentCode random
      this.props.accessToken,
      this.callbackInfoGot,
      this.callbackServerError,
      this.callbackUnauthorizedError
    );*/
    GetNews(
      this.props.accessToken,
      this.callbackInfoGot,
      this.callbackServerError,
      this.callbackUnauthorizedError
    );
  }

  render() {
    return (
      <div>
        {this.state.serverError !== null && (
          <div className="alert alert-danger" role="alert">
            {this.state.serverError.error} : {this.state.serverError.message}
          </div>
        )}
        <nav className="navbar navbar-light bg-light">
          <a className="navbar-brand" href="/">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Oauth_logo.svg"
              width="48"
              height="48"
              className="d-inline-block align-top"
              alt="OAuth"
            />
            &nbsp;&nbsp;<span style={{ fontSize: 36 }}>OAuth 2.0</span>
          </a>
        </nav>
        <div>
          {this.props.refreshError && (
            <Jumbotron className="jumbo">
              <h1>Request FAILED</h1>
              <div className="alert alert-danger" role="alert">
                {this.props.error.error} :
                {this.props.error.message.substring(0, 20)}
              </div>
              <ul>
                <li>
                  This error is occurred because your request to the Resource
                  Server was sent with the REFRESH_TOKEN procedure
                  <b> disabled</b>, this means that your access_token is
                  expired.
                </li>
              </ul>
              <br />
              <ul>
                <li>
                  Try again sending a request which implements the REFRESH_TOKEN
                  procedure.
                </li>
              </ul>
              <Container className="no-gutters">
                <Row className="no-gutters">
                  <Col className="col-6 col-md-4 text-success">
                    <b>Refresh ENABLED</b>
                    <br />
                    <b>You will get a correct result!</b>
                  </Col>
                </Row>
                <Row className="no-gutters">
                  <Col className="col-6 col-md-4 text-danger">
                    <button
                      onClick={() => {
                        this.doGet(this.props.accessToken);
                      }}
                      className="btn btn-success btn-space-result btn-custom"
                    >
                      <span>Make a new request</span>
                    </button>
                  </Col>
                </Row>
              </Container>
            </Jumbotron>
          )}
          {!this.props.refreshed && !this.props.refreshError && (
            <Jumbotron className="jumbo">
              <h1>Welcome user</h1>
              <ul>
                <li>
                  The OAuth password grant flow is used, so the application
                  exchages the user's username and password for an access token.
                </li>
                <li>
                  A request like this, with the right parameters, was sent to
                  the Authorization Server:
                </li>
              </ul>
              <pre className="code-block">
                POST /oauth/token HTTP/1.1 <br />
                Host: authorization-server.com <br />
                <br />
                grant_type=password <br />
                &amp;username=user@gmail.com
                <br />
                &amp;password=pass <br />
                &amp;client_id=xxxxxxxxxx
                <br />
                &amp;client_secret=xxxxxxxxxx
              </pre>
              <ul>
                <li>
                  The Authorization Server validates the information, then
                  return an Access Token with some additional properties about
                  the authorization.
                </li>
                <li>Here you are your access token:</li>
              </ul>
              <pre className="code-block">
                <span className="token-punctuation">&#123;</span>
                <br />
                <span className="token-attr-name">access_token</span>
                <span className="token-punctuation">="</span>
                <span className="token-attr-value">
                  {this.props.accessToken}
                </span>
                <br />
                <span className="token-punctuation">&#125;</span>
              </pre>
              <ul>
                <li>
                  With the access token obtained you can make a request to the
                  Resource Server to access the protected resources.
                </li>
                <li>
                  In this scenario, if you have the right authorization you will
                  get information about <b>SECRET AGENT 007</b>
                </li>
              </ul>
              <p className="request-p">
                <img
                  className="mb-4"
                  src="https://cdn2.iconfinder.com/data/icons/whcompare-isometric-web-hosting-servers/50/secure-server-512.png"
                  alt="ResourceServer"
                  width="256"
                  height="256"
                />
                <br />
                <button
                  onClick={() => {
                    this.doGet(this.props.accessToken);
                  }}
                  className="btn btn-primary btn-space btn-custom"
                >
                  <span>Make the request</span>
                </button>
              </p>
            </Jumbotron>
          )}
          {this.props.refreshed &&
            !this.props.dataRetrieved &&
            this.doGet(this.props.accessToken)}
          {this.props.refreshed && this.props.dataRetrieved && (
            <Jumbotron className="jumbo small-margin-jumbo">
              <h1>Welcome again user</h1>
              <ul>
                <li>
                  Here you are your access token (REFRESHED) used in your
                  request to the Resource Server:
                </li>
              </ul>
              <pre className="code-block">
                <span className="token-punctuation">&#123;</span>
                <br />
                <span className="token-attr-name">access_token</span>
                <span className="token-punctuation">="</span>
                <span className="token-attr-value">
                  {this.props.accessToken}
                </span>
                <span className="token-punctuation">"</span>
                <br />
                <span className="token-punctuation">&#125;</span>
              </pre>
              <ul>
                <li>
                  A request like this, with the right parameters, was sent to
                  the Authorization Server to REFRESH the token:
                </li>
              </ul>
              <pre className="code-block">
                POST /oauth/token HTTP/1.1 <br />
                Host: authorization-server.com <br />
                <br />
                grant_type=refreh_token <br />
                &amp;refresh_token="Here the refresh token given with the
                original access token"
                <br />
                &amp;client_id=xxxxxxxxxx
                <br />
                &amp;client_secret=xxxxxxxxxx
              </pre>
              <ul>
                <li>
                  With the new access token <b>REFRESHED</b> you can make
                  requests to the Resource Server to access the protected
                  resources resolving the problem of the token expiration.
                </li>
              </ul>
            </Jumbotron>
          )}
        </div>
      </div>
    );
  }
}

export default WelcomePage;
