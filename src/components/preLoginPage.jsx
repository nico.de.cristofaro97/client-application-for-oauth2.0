import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import "../customCSS/floating-labels.css";
import { FacebookLoginButton } from "react-social-login-buttons";
import { GoogleLoginButton } from "react-social-login-buttons";
import { Button } from "react-bootstrap";
import StartPage from "./startPage";

class PreLoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showComponent: false
    };

    this.onButtonClick = this.onButtonClick.bind(this);
  }

  onButtonClick() {
    this.setState({
      showComponent: true
    });
  }

  render() {
    return (
      <div>
        {this.state.showComponent && <StartPage />}
        {!this.state.showComponent && (
          <Formik
            render={({ errors, status, touched }) => (
              <Form className="form-signin">
                <div className="text-center mb-4">
                  <img
                    className="mb-4"
                    src="https://upload.wikimedia.org/wikipedia/commons/9/9c/La_Repubblica_logo.png"
                    alt="OAuth"
                    width="224"
                    height="50"
                  />
                  <h2 className="h3 mb-3 font-weight-normal">Registrati.</h2>
                </div>
                <div className="form-label-group">
                  <Field
                    name="email"
                    type="text"
                    id="inputEmail"
                    className="form-control"
                    placeholder="Email address"
                    autoFocus
                  />
                  <label htmlFor="inputEmail">Email address</label>
                </div>
                <div className="form-label-group">
                  <Field
                    name="data_nascita"
                    type="text"
                    id="input_data_nascita"
                    className="form-control"
                    placeholder="Data di nascita"
                  />
                  <label htmlFor="inputDataNascita">Data di Nascita</label>
                </div>
                <div className="form-label-group">
                  <Field
                    name="città"
                    type="text"
                    id="inputCitta"
                    className="form-control"
                    placeholder="Città"
                  />
                  <label htmlFor="inputCittà">Città</label>
                </div>
                <div className="form-label-group">
                  <Field
                    name="password"
                    type="password"
                    id="inputPassword"
                    className="form-control"
                    placeholder="Password"
                  />
                  <label htmlFor="inputPassword">Password</label>
                </div>
                <div className="form-label-group">
                  <Field
                    name="conferma_password"
                    type="password"
                    id="inputPassword"
                    className="form-control"
                    placeholder="Conferma Password"
                  />
                  <label htmlFor="inputConfermaPassword">
                    Conferma Password
                  </label>
                </div>
                <button className="btn btn-primary loginBtn" type="submit">
                  Sign in
                </button>
                <button type="reset" className="btn btn-secondary loginBtn">
                  Reset
                </button>
                <h5 className="mb-3 mt-3 text-center">
                  Utilizza la tua utenza social (OAuth):
                </h5>
                <Button
                  onClick={this.onButtonClick}
                  variant="dark"
                  size="lg"
                  className="authButton"
                >
                  My Authorization Server
                </Button>
                <FacebookLoginButton />
                <GoogleLoginButton />
                <p className="mt-5 mb-3 text-muted text-center">
                  &copy; 2019-2020
                </p>
              </Form>
            )}
          />
        )}
      </div>
    );
  }
}

export default PreLoginPage;
