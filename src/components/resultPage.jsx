import React, { Component } from "react";
//import { GetExtraInfo } from "../server/resourceServer";
//import { GetSecretAgent } from "../server/resourceServer";
import { GetNews } from "../server/resourceServer";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Jumbotron from "react-bootstrap/Jumbotron";
import "../customCSS/result.css";
import { Navbar } from "react-bootstrap";

import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const cardWidth = {
  maxWidth: 345,
  display: "inline-block",
  marginRight: 15
};

class ResultPage extends Component {
  constructor(props) {
    super(props);
    this.callbackServerError = this.callbackServerError.bind(this);
    this.callbackUnauthorizedError = this.callbackUnauthorizedError.bind(this);
    this.callbackInfoGot = this.callbackInfoGot.bind(this);

    this.state = {
      seconds: this.props.expireIn,
      serverError: null
    };
  }

  callbackServerError(data) {
    //console.log(Object.keys(data));
    this.setState({
      serverError: {
        error: data.error,
        message: data.error_description
      }
    });
  }

  callbackUnauthorizedError(data) {
    //error 401 Unauthorized
    this.props.onUnauthorizedError(data, this.state.refr);
  }

  callbackInfoGot(info) {
    console.log(info);
    this.props.onGetInfo(info);
  }

  anotherRequest(refresh) {
    //this was the general request to understand the flow
    //this request get from the access token the extraInfo->siteId
    /*GetExtraInfo(
      this.props.accessToken,
      this.callbackInfoGot,
      this.callbackServerError,
      this.callbackUnauthorizedError
    );*/
    //set the attribute that tells if it is sent a request with refresh procedure enabled(true) or disabled(false)
    this.setState({
      refr: refresh
    });

    //this new request to the resource server get the data of a secret agent
    /*GetSecretAgent(
      "007", // agentCode random
      this.props.accessToken,
      this.callbackInfoGot,
      this.callbackServerError,
      this.callbackUnauthorizedError
    );*/
    GetNews(
      this.props.accessToken,
      this.callbackInfoGot,
      this.callbackServerError,
      this.callbackUnauthorizedError
    );
  }

  tick() {
    if (this.state.seconds === 0) clearInterval(this.interval);
    else {
      this.setState(prevState => ({
        seconds: prevState.seconds - 1
      }));
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div>
        {this.state.serverError !== null && (
          <div className="alert alert-danger" role="alert">
            {this.state.serverError.error} : {this.state.serverError.message}
          </div>
        )}
        {!this.props.refreshed && (
          <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="/">
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Oauth_logo.svg"
                width="48"
                height="48"
                className="d-inline-block align-top"
                alt="OAuth"
              />
              &nbsp;&nbsp;<span style={{ fontSize: 36 }}>OAuth 2.0</span>
            </a>
          </nav>
        )}
        <Jumbotron className="jumbo">
          <h2>
            Access to the <b>PROTECTED RESOURCE</b> done correctly.
          </h2>
          <ul>
            <li>This is your result:</li>
          </ul>
          {/*<pre className="code-block">
            <span className="token-punctuation">&#123;</span>
            <br />
            <span className="token-attr-name">{this.props.dataNames[0]}</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">
              {this.props.data.agentCode}
            </span>
            <span className="token-punctuation">"</span>
            <br />
            <span className="token-attr-name">{this.props.dataNames[1]}</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">
              {this.props.data.agentName}
            </span>
            <span className="token-punctuation">"</span>
            <br />
            <span className="token-attr-name">{this.props.dataNames[2]}</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">
              {this.props.data.workingArea}
            </span>
            <span className="token-punctuation">"</span>
            <br />
            <span className="token-attr-name">{this.props.dataNames[3]}</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">
              {this.props.data.experienceYears}
            </span>
            <span className="token-punctuation">"</span>
            <br />
            <span className="token-attr-name">{this.props.dataNames[4]}</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">{this.props.data.phoneNo}</span>
            <span className="token-punctuation">"</span>
            <br />
            <span className="token-attr-name">{this.props.dataNames[5]}</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">
              {this.props.data.department.depName}
            </span>
            <span className="token-punctuation">"</span>
            <br />
            {/*<span className="token-attr-name">site_id</span>
            <span className="token-punctuation">="</span>
            <span className="token-attr-value">{this.props.extra}</span>
            <span className="token-punctuation">"</span>}  qui finiva un altro commento
            <span className="token-punctuation">&#125;</span>
          </pre>*/}
          <Navbar
            bg="light"
            variant="light"
            className="rounded mb-0 custom-nav-logo"
          >
            <Navbar.Brand href="#home">
              <img
                alt="laRepubblica_logo"
                src="https://upload.wikimedia.org/wikipedia/commons/9/9c/La_Repubblica_logo.png"
                width="200"
                height="50"
                className="d-inline-block align-top"
              />
            </Navbar.Brand>
          </Navbar>
          <pre className="code-block">
            <Card style={cardWidth}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt="Dog test"
                  height="200"
                  image={this.props.data[0].image}
                  title="Dog test"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {this.props.data[0].title}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {this.props.data[0].shortDescription}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Share
                </Button>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
            <Card style={cardWidth}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt="Dog test"
                  height="200"
                  image={this.props.data[1].image}
                  title="Dog test"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {this.props.data[1].title}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {this.props.data[1].shortDescription}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Share
                </Button>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
            <Card style={cardWidth}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt="Dog test"
                  height="200"
                  image={this.props.data[2].image}
                  title="Dog test"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {this.props.data[2].title}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {this.props.data[2].shortDescription}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Share
                </Button>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
          </pre>
          <p>
            Number of requests to the Resource Server with the same access_token
            -> <b className="num-req text-primary">{this.props.numRequest}</b>
          </p>
          {!this.props.refreshed && (
            <React.Fragment>
              <p style={{ color: "red" }}>
                ***Note that your access_token has a <b>LIFETIME</b> so once it
                expires you will no longer be able to make requests to the
                resource Server.
              </p>
              <ul>
                <li>
                  To handle the token expiration there is a procedure to use
                  <b> refresh_token</b> given with the initial access_token to
                  obtain a new and valid access_token.
                </li>
              </ul>
              <b>
                Remaining time to the token expiration ->
                {this.state.seconds}
              </b>
              <br />
              <button
                onClick={() => {
                  this.anotherRequest(true);
                }}
                className="btn btn-primary btn-space-result btn-custom"
              >
                <span>Make another request</span>
              </button>
            </React.Fragment>
          )}
          {this.props.refreshed && (
            <React.Fragment>
              <ul>
                <li>
                  Now you can choose, the buttons will do the same request to
                  the Resaource Server, so until the access_token is still valid
                  they will get the same result.
                </li>
                <li>
                  But when the access_token expires you will note the
                  difference:
                </li>
                <ul>
                  <li>
                    the first button allow to send a request with the
                    REFRESH_TOKEN procedure <b>enabled</b>
                  </li>
                  <li>
                    the second button allow to send a request with the
                    REFRESH_TOKEN procedure <b>disabled</b>
                  </li>
                </ul>
                <li>
                  So the expected result when you click the first button is the
                  correct result while the second button will return an error
                  because the request will try to get the protected resource
                  with an access_token expired.
                </li>
              </ul>
              <b>
                Remaining time to the token expiration ->
                {this.state.seconds}
              </b>
              <br />
              <br />
              <Container className="no-gutters">
                <Row className="no-gutters">
                  <Col className="col-6 col-md-4 text-success">
                    <b>Refresh ENABLED</b>
                    <br />
                    <b>You will get a correct result!</b>
                  </Col>
                  <Col className="col-6 col-md-4 text-danger">
                    <b>Refresh DISABLED</b>
                    <br />
                    <b>You will get an error after the token expiration!</b>
                  </Col>
                </Row>
                <Row className="no-gutters-button">
                  <Col className="col-6 col-md-4">
                    <button
                      onClick={() => {
                        this.anotherRequest(true);
                      }}
                      className="btn btn-success btn-space-result btn-custom success"
                    >
                      <span>Make a new request</span>
                    </button>
                  </Col>
                  <Col className="col-6 col-md-4">
                    <button
                      onClick={() => {
                        this.anotherRequest(false);
                      }}
                      className="btn btn-danger btn-space-result btn-custom error"
                    >
                      <span>Make a new request</span>
                    </button>
                  </Col>
                </Row>
              </Container>
            </React.Fragment>
          )}
        </Jumbotron>
      </div>
    );
  }
}

export default ResultPage;
