import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import "../customCSS/floating-labels.css";
import { Login } from "../server/authServer";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.callbackTokenGot = this.callbackTokenGot.bind(this);
    this.callbackServerError = this.callbackServerError.bind(this);

    this.state = {
      serverError: null
    };
  }

  callbackTokenGot(info) {
    //console.log(info);
    this.props.onGetToken(info);
  }

  callbackServerError(data) {
    this.setState({
      serverError: {
        error: data.error,
        message: data.error_description
      }
    });
  }

  render() {
    return (
      <div>
        {this.state.serverError !== null && (
          <div className="alert alert-danger" role="alert">
            {this.state.serverError.error} : {this.state.serverError.message}
          </div>
        )}
        <br />
        <Formik
          initialValues={{
            email: "",
            password: ""
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
            password: Yup.string()
              .min(4, "Password must be at least 4 characters")
              .required("Password is required")
          })}
          onSubmit={(fields, { setSubmitting }) => {
            this.setState({
              serverError: null
            });
            setTimeout(() => {
              Login(fields, this.callbackTokenGot, this.callbackServerError);
              setSubmitting(false);
            }, 60);
            //alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4))
            //console.log(fields);
          }}
          render={({ errors, status, touched }) => (
            <Form className="form-signin">
              <div className="text-center mb-4">
                <img
                  className="mb-4"
                  src="https://upload.wikimedia.org/wikipedia/commons/d/d2/Oauth_logo.svg"
                  alt="OAuth"
                  width="124"
                  height="124"
                />
                <h1 className="h3 mb-3 font-weight-normal">
                  OAuth 2.0 Protocol
                </h1>
                <p>
                  Form controls for the Resource Owner Password Credentials
                  OAuth flow.
                </p>
              </div>
              <div className="form-label-group">
                <Field
                  name="email"
                  type="text"
                  id="inputEmail"
                  className={
                    "form-control" +
                    (errors.email && touched.email ? " is-invalid" : "")
                  }
                  placeholder="Email address"
                  required
                  autoFocus
                />
                <label htmlFor="inputEmail">Email address</label>
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-label-group">
                <Field
                  name="password"
                  type="password"
                  id="inputPassword"
                  className={
                    "form-control" +
                    (errors.password && touched.password ? " is-invalid" : "")
                  }
                  placeholder="Password"
                  required
                />
                <label htmlFor="inputPassword">Password</label>
                <ErrorMessage
                  name="password"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              {/*
              <div className="checkbox mb-3">
                <label>
                  <input type="checkbox" value="remember-me" /> Remember me
                </label>
              </div>*/}
              <button className="btn btn-primary loginBtn" type="submit">
                Sign in
              </button>
              <button type="reset" className="btn btn-secondary loginBtn">
                Reset
              </button>
              <p className="mt-5 mb-3 text-muted text-center">
                &copy; 2019-2020
              </p>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default LoginPage;
