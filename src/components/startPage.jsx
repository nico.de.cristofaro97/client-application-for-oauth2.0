import React, { Component } from "react";
import LoginPage from "./loginPage";
import WelcomePage from "./welcomePage";
import ResultPage from "./resultPage";
import { RefreshToken } from "../server/authServer";

class StartPage extends Component {
  constructor(props) {
    super(props);
    this.handleToken = this.handleToken.bind(this);
    this.handleInfo = this.handleInfo.bind(this);
    this.callbackServerError = this.callbackServerError.bind(this);
    this.handleUnauthorizedError = this.handleUnauthorizedError.bind(this);
    this.callbackRefreshToken = this.callbackRefreshToken.bind(this);

    this.state = {
      accessToken: null,
      tokenType: null,
      refreshToken: null,
      expireIn: null,
      scope: null,
      serverError: null,
      tokenGotNotRefreshed: false,
      numRequest: 0,
      refreshed: false,
      refreshCount: false
    };
  }

  callbackRefreshToken(info) {
    this.setState({
      accessToken: info.access_token,
      tokenType: info.token_type,
      refreshToken: info.refresh_token,
      expireIn: info.expires_in,
      scope: info.scope,

      refreshed: true,
      refreshCount: true,
      dataRetrieved: null, //because extraInfo has to be regenerated
      tokenGotNotRefreshed: false,
      refreshError: false,
      serverError: null
    });
  }

  callbackServerError(data) {
    //console.log(Object.keys(data));
    this.setState({
      serverError: {
        error: data.error,
        message: data.error_description
      }
    });
    //console.log(this.state.serverError);
  }

  handleUnauthorizedError(data, refr) {
    //console.log(refr + " here");
    //console.log(data);

    //error 401 Unauthorized
    //check if it is an expired token
    //console.log(data.error_description.substring(0, 20));
    if (
      data.error === "invalid_token" &&
      data.error_description.substring(0, 20) === "Access token expired"
    ) {
      if (refr !== undefined && !refr) {
        //console.log("yes");

        //request sent with refresh procedure disabled
        //set an attribute to show a customized WelcomPage with error description
        this.setState({
          refreshError: true,
          serverError: {
            error: data.error,
            message: data.error_description
          }
        });
      } else {
        //make another request to AuthServer to refresh token
        RefreshToken(
          this.state.refreshToken,
          this.callbackRefreshToken,
          this.callbackServerError
        );
      }
    } else {
      //It is another kind of error
      this.callbackServerError(data);
    }
  }

  handleInfo(info) {
    this.setState({
      dataRetrieved: info,
      dataRetrievedNames: Object.keys(info),
      tokenGotNotRefreshed: false //to not show WelcomePage
    });
    //console.log(this.state.dataRetrieved);
    //console.log(this.state.dataRetrievedNames);
    if (this.state.refreshCount) {
      this.setState({
        numRequest: 1,
        refreshCount: false
      });
    } else {
      this.setState({ numRequest: this.state.numRequest + 1 });
    }
  }

  handleToken(data) {
    this.setState({
      accessToken: data.access_token,
      tokenType: data.token_type,
      refreshToken: data.refresh_token,
      expireIn: data.expires_in,
      scope: data.scope,
      tokenGotNotRefreshed: true
    });
  }

  render() {
    return (
      <div>
        {this.state.serverError !== null && !this.state.refreshError && (
          <div className="alert alert-danger" role="alert">
            {this.state.serverError.error} : {this.state.serverError.message}
          </div>
        )}
        {this.state.refreshError && (
          <WelcomePage
            accessToken={this.state.accessToken}
            refreshError={this.state.refreshError}
            error={this.state.serverError}
            onGetInfo={this.handleInfo}
            onUnauthorizedError={this.handleUnauthorizedError}
          />
        )}
        {!this.state.refreshed &&
          this.state.dataRetrieved &&
          !this.state.refreshError && (
            <ResultPage
              data={this.state.dataRetrieved}
              dataNames={this.state.dataRetrievedNames}
              expireIn={this.state.expireIn}
              accessToken={this.state.accessToken}
              onGetInfo={this.handleInfo}
              numRequest={this.state.numRequest}
              onUnauthorizedError={this.handleUnauthorizedError}
            />
          )}
        {this.state.tokenGotNotRefreshed && (
          <WelcomePage
            accessToken={this.state.accessToken}
            onGetInfo={this.handleInfo}
            onUnauthorizedError={this.handleUnauthorizedError}
          />
        )}
        {this.state.refreshed && !this.state.dataRetrieved && (
          <WelcomePage
            accessToken={this.state.accessToken}
            onGetInfo={this.handleInfo}
            onUnauthorizedError={this.handleUnauthorizedError}
            refreshed={this.state.refreshed}
          />
        )}
        {this.state.refreshed &&
          this.state.dataRetrieved &&
          !this.state.refreshError && (
            <WelcomePage
              accessToken={this.state.accessToken}
              onGetInfo={this.handleInfo}
              onUnauthorizedError={this.handleUnauthorizedError}
              refreshed={this.state.refreshed}
              dataRetrieved={this.state.dataRetrieved}
            />
          )}
        {this.state.refreshed &&
          this.state.dataRetrieved &&
          !this.state.refreshError && (
            <ResultPage
              data={this.state.dataRetrieved}
              dataNames={this.state.dataRetrievedNames}
              expireIn={this.state.expireIn}
              accessToken={this.state.accessToken}
              onGetInfo={this.handleInfo}
              numRequest={this.state.numRequest}
              onUnauthorizedError={this.handleUnauthorizedError}
              refreshed={this.state.refreshed}
            />
          )}
        {!this.state.serverError &&
          !this.state.tokenGotNotRefreshed &&
          !this.state.refreshed &&
          !this.state.dataRetrieved && (
            <LoginPage onGetToken={this.handleToken} />
          )}
      </div>
    );
  }
}

export default StartPage;
