import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import StartPage from "./components/startPage";
import PreLoginPage from "./components/preLoginPage";
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(<PreLoginPage />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
