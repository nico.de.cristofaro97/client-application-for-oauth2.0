const server = "http://localhost:8081";

export const GetExtraInfo = function(token, success, error, unauthorized) {
  let headers = new Headers({
    Authorization: "Bearer " + token
  });

  fetch(`${server}/additionalInfo`, {
    method: "GET",
    headers: headers
  })
    .then(res => {
      if (res.status === 200) {
        res.json().then(data => {
          success(data);
        });
      } else if (res.status === 401) {
        res.json().then(data => {
          unauthorized(data);
        });
      } else {
        res.json().then(data => {
          error(data);
        });
      }
    })
    .catch(console.log);
};

export const GetSecretAgent = function(
  agentCode,
  token,
  success,
  error,
  unauthorized
) {
  let headers = new Headers({
    Authorization: "Bearer " + token
  });

  fetch(`${server}/agents/` + agentCode, {
    method: "GET",
    headers: headers
  })
    .then(res => {
      if (res.status === 200) {
        res.json().then(data => {
          success(data);
        });
      } else if (res.status === 401) {
        res.json().then(data => {
          unauthorized(data);
        });
      } else {
        res.json().then(data => {
          error(data);
        });
      }
    })
    .catch(console.log);
};

export const GetNews = function(token, success, error, unauthorized) {
  let headers = new Headers({
    Authorization: "Bearer " + token
  });

  fetch(`${server}/news/`, {
    method: "GET",
    headers: headers
  })
    .then(res => {
      if (res.status === 200) {
        res.json().then(data => {
          success(data);
        });
      } else if (res.status === 401) {
        res.json().then(data => {
          unauthorized(data);
        });
      } else {
        res.json().then(data => {
          error(data);
        });
      }
    })
    .catch(console.log);
};
