const clientId = "clientId";
const secret = "secret";

const server = "http://localhost:8085";

export const Login = function(fields, success, error) {
  let headers = new Headers({
    Authorization:
      "Basic " + Buffer.from(clientId + ":" + secret).toString("base64")
  });

  fetch(
    `${server}/oauth/token?grant_type=password&username=` +
      fields.email +
      `&password=` +
      fields.password,
    {
      method: "POST",
      headers: headers
    }
  )
    .then(res => {
      if (res.status === 200) {
        res.json().then(data => {
          success(data);
        });
      } else {
        res.json().then(data => {
          error(data);
        });
      }
    })
    .catch(console.log);
};

export const RefreshToken = function(refresh_token, success, error) {
  let headers = new Headers({
    Authorization:
      "Basic " + Buffer.from(clientId + ":" + secret).toString("base64")
    //'Accept': 'application/json',
    //'Content-Type': 'application/json',
  });

  fetch(
    `${server}/oauth/token?grant_type=refresh_token&refresh_token=` +
      refresh_token,
    {
      method: "POST",
      headers: headers
    }
  )
    .then(res => {
      if (res.status === 200) {
        res.json().then(data => {
          success(data);
        });
      } else {
        res.json().then(data => {
          error(data);
        });
      }
    })
    .catch(console.log);
};
